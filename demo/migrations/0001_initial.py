# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField(null=True, blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='demo.Category', null=True)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('code', models.CharField(max_length=50, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('image', models.ImageField(null=True, upload_to=b'foto', blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('stocks', models.IntegerField(default=0, blank=True)),
                ('priceAustria', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=6, blank=True, null=True, verbose_name=b'Recomended Price')),
                ('category', mptt.fields.TreeForeignKey(blank=True, to='demo.Category', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
