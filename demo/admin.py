from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from .models import Category, Product

# Register your models here.
class CategoryAdmin(MPTTModelAdmin):
    fields = ['name', 'description', 'parent']
    list_display = ('name', )

    mptt_level_indent = 15

admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    ordering = ('code',)
    fieldsets = [
        (None, {'fields': ['code', 'stocks', 'image', 'category']}),
        (None, {'fields': ['name', 'description',]}),
        ('Prices', {'fields': ['priceAustria'], 'classes': ['grp-collapse  grp-closed']}),
    ]
    # change_list_template = 'admin/product_change_list.html'
    list_display = ('code', 'name', 'category')
    search_fields = ['code']

    list_filter = ('name',)


admin.site.register(Product, ProductAdmin)