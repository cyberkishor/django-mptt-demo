__author__ = 'kishor'
from django import forms
from mptt.forms import TreeNodeChoiceField

from .models import Category

class CategoryForm(forms.Form):
    category_model = forms.ModelChoiceField(queryset=Category.objects.all())
    category_mptt = TreeNodeChoiceField(queryset=Category.objects.all(), level_indicator=u'>')