from django.shortcuts import render
from django.views.generic.list import ListView
import datetime
from .models import Category, Product
from .forms import CategoryForm

class CategoryListView(ListView):
    template_name = 'category_list.html'
    model = Category

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['now'] = datetime.datetime.now()
        context['category_form'] = CategoryForm()
        context['products'] = Product.objects.all()
        return context